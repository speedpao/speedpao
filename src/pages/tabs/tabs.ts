import { Component } from '@angular/core';

import { PadariasPage } from '../padarias/padarias';
import { PedidosPage } from '../pedidos/pedidos';
import { CarrinhoPage } from '../carrinho/carrinho';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  padarias = PadariasPage;
  pedidos = PedidosPage;
  carrinho = CarrinhoPage;

  constructor() {

  }
}
