import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PadariasPage } from './padarias';

@NgModule({
  declarations: [
    PadariasPage,
  ],
  imports: [
    IonicPageModule.forChild(PadariasPage),
  ],
  exports: [
    PadariasPage
  ]
})
export class PadariasPageModule {}
